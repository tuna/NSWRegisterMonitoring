#ifndef NSWREGISTERMONITORING_NSWREGISTERMONITORINGRC_H_
#define NSWREGISTERMONITORING_NSWREGISTERMONITORINGRC_H_

#include <memory>

#include <ipc/partition.h>
#include <is/infodictionary.h>

#include <RunControl/RunControl.h>

// #include "NSWRegisterMonitoring/NSWRegisterMonitoring.h"

namespace daq::rc {
  class SubTransitionCmd;
  class TransitionCmd;
  class UserCmd;
}

namespace nsw {
class NSWRegisterMonitoringRc: public daq::rc::Controllable {
 public:
    // override only the needed methods
    explicit NSWRegisterMonitoringRc(bool simulation = false);
    virtual ~NSWRegisterMonitoringRc() noexcept {}

    //! Connects to config database/ or reads file based config database
    //! Reads the names of front ends that should be configured and constructs
    //! FEBConfig objects in the map m_frontends
    void configure(const daq::rc::TransitionCmd& cmd) override;

    void connect(const daq::rc::TransitionCmd& cmd) override;

    void prepareForRun(const daq::rc::TransitionCmd& cmd) override;

    void stopRecording(const daq::rc::TransitionCmd& cmd) override;

    void unconfigure(const daq::rc::TransitionCmd& cmd) override;

    void disconnect(const daq::rc::TransitionCmd& cmd) override;

    void user(const daq::rc::UserCmd& cmd) override;

    // void onExit(daq::rc::FSM_STATE) noexcept override;

    //! Used to syncronize ROC/VMM config
    void subTransition(const daq::rc::SubTransitionCmd&) override;

    //! Handle Config
    bool simulationFromIS();

 private:
    // std::unique_ptr<NSWRegisterMonitoring> m_NSWRegisterMonitoring;
    bool m_simulation;
    bool m_simulation_lock{false};

    IPCPartition m_ipcpartition;
    std::unique_ptr<ISInfoDictionary> is_dictionary;

};
}  // namespace nsw
#endif  // NSWREGISTERMONITORING_NSWREGISTERMONITORINGRC_H_
