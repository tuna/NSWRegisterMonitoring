#include "NSWRegisterMonitoring/NSWRegisterMonitoringRc.h"

#include <utility>
#include <string>
#include <memory>

// Header to the RC online services
#include <RunControl/Common/OnlineServices.h>
#include <RunControl/Common/RunControlCommands.h>

#include <is/infodynany.h>

#include "NSWRegisterMonitoringDal/NSWRegisterMonitoringApplication.h"

#include <ers/ers.h>

nsw::NSWRegisterMonitoringRc::NSWRegisterMonitoringRc(bool simulation) : m_simulation {simulation} {
    ERS_LOG("Constructing NSWRegisterMonitoringRc instance");
    if (m_simulation) {
        ERS_INFO("Running in simulation mode, no configuration will be sent");
        m_simulation_lock = true;
    }
}

bool nsw::NSWRegisterMonitoringRc::simulationFromIS() {
    // Grab the simulation bool from IS
    // Can manually write to this variable from the command line:
    // > is_write -p part-BB5-Calib -n Setup.NSW.simulation -t Boolean -v 1 -i 0
    // if (is_dictionary->contains("Setup.NSW.simulation") ){
    //     ISInfoDynAny any;
    //     is_dictionary->getValue("Setup.NSW.simulation", any);
    //     auto val = any.getAttributeValue<bool>(0);
    //     ERS_INFO("Simulation from IS: " << val);
    //     return val;
    // }
    return false;
}

void nsw::NSWRegisterMonitoringRc::configure(const daq::rc::TransitionCmd&) {
    ERS_INFO("Start");
    // // Retrieving the configuration db
    // daq::rc::OnlineServices& rcSvc = daq::rc::OnlineServices::instance();
    // const daq::core::RunControlApplicationBase& rcBase = rcSvc.getApplication();
    // const nsw::dal::NSWRegisterMonitoringApplication* nswregistermonitoringApp = rcBase.cast<nsw::dal::NSWRegisterMonitoringApplication>();

    // // Retrieve the ipc partition
    // m_ipcpartition = rcSvc.getIPCPartition();

    // // Get the IS dictionary for the current partition
    // is_dictionary = std::make_unique<ISInfoDictionary>(m_ipcpartition);

    // if (!m_simulation_lock) {
    //     m_simulation = simulationFromIS();
    // }

    // m_NSWRegisterMonitoring = std::make_unique<NSWRegisterMonitoring>(m_simulation);
    // m_NSWRegisterMonitoring->readConf(nswregistermonitoringApp);
    ERS_LOG("End");
}

void nsw::NSWRegisterMonitoringRc::connect(const daq::rc::TransitionCmd&) {
    ERS_INFO("Start");
    // m_NSWRegisterMonitoring->configureRc();
    ERS_LOG("End");
}

void nsw::NSWRegisterMonitoringRc::prepareForRun(const daq::rc::TransitionCmd&) {
    ERS_LOG("Start");
    // m_NSWRegisterMonitoring->startRc();
    ERS_LOG("End");
}

void nsw::NSWRegisterMonitoringRc::stopRecording(const daq::rc::TransitionCmd&) {
    ERS_LOG("Start");
    // m_NSWRegisterMonitoring->stopRc();
    ERS_LOG("End");
}

void nsw::NSWRegisterMonitoringRc::disconnect(const daq::rc::TransitionCmd&) {
    ERS_INFO("Start");
    // m_NSWRegisterMonitoring->unconfigureRc();
    ERS_INFO("End");
}

void nsw::NSWRegisterMonitoringRc::unconfigure(const daq::rc::TransitionCmd&) {
    ERS_INFO("Start");
    ERS_INFO("End");
}

void nsw::NSWRegisterMonitoringRc::user(const daq::rc::UserCmd& usrCmd) {
  ERS_LOG("User command received: " << usrCmd.commandName());
  if (usrCmd.commandName() == "enableVmmCaptureInputs")
  {
    // m_NSWRegisterMonitoring->enableVmmCaptureInputs();
  }
}

void nsw::NSWRegisterMonitoringRc::subTransition(const daq::rc::SubTransitionCmd& cmd) {
    auto main_transition = cmd.mainTransitionCmd();
    auto sub_transition = cmd.subTransition();

    ERS_LOG("Sub transition received: " << sub_transition << " (mainTransition: " << main_transition << ")");
}


